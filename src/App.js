import React from 'react';
import './App.css';

function App() {
  const chats = []

  const send = () => {
    chats.push(document.getElementById("chatBox").value)
    document.getElementById("chatBox").value = ""
    localStorage.setItem("chats", JSON.stringify(chats))
  }

  const showMessages = () => {
    const messages = localStorage.getItem("chats")
    console.log(JSON.parse(messages))
  }

  return (
    <div className="App">
      <header className='PageHeader'>Chat to localStorage assignment</header>
      <div className='ParagraphContainer'>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet cursus sit amet dictum sit amet justo. Sit amet commodo nulla facilisi. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Gravida in fermentum et sollicitudin ac. Vitae congue eu consequat ac felis donec et odio pellentesque. Aliquam eleifend mi in nulla posuere. Commodo viverra maecenas accumsan lacus vel. Tortor consequat id porta nibh. Elementum sagittis vitae et leo duis ut diam. Mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor. Iaculis eu non diam phasellus vestibulum lorem sed.</p>
        <p>Egestas dui id ornare arcu odio ut sem nulla. Sit amet consectetur adipiscing elit ut. Pharetra pharetra massa massa ultricies mi. Felis imperdiet proin fermentum leo. Ut diam quam nulla porttitor massa id neque aliquam. Sed blandit libero volutpat sed cras ornare. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Aliquam sem fringilla ut morbi. Eu sem integer vitae justo. Nulla malesuada pellentesque elit eget gravida. Eget velit aliquet sagittis id consectetur. Egestas dui id ornare arcu odio ut. Dis parturient montes nascetur ridiculus mus mauris vitae. Dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Pellentesque dignissim enim sit amet. Ipsum dolor sit amet consectetur adipiscing. Ultrices tincidunt arcu non sodales neque.</p>
        <p>Neque viverra justo nec ultrices dui sapien eget mi. Nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Laoreet non curabitur gravida arcu ac tortor dignissim convallis. Dignissim cras tincidunt lobortis feugiat vivamus. Euismod quis viverra nibh cras pulvinar mattis nunc sed. Id aliquet lectus proin nibh. Tellus in metus vulputate eu scelerisque. Duis at consectetur lorem donec massa sapien faucibus et. Ultricies integer quis auctor elit. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus. Senectus et netus et malesuada fames. Ac tortor vitae purus faucibus ornare suspendisse sed nisi. Magna etiam tempor orci eu lobortis elementum. Suspendisse potenti nullam ac tortor vitae purus. Est pellentesque elit ullamcorper dignissim cras. Volutpat est velit egestas dui id ornare. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Viverra maecenas accumsan lacus vel facilisis volutpat est. Feugiat scelerisque varius morbi enim nunc faucibus a.</p>
        <p>Est lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Ipsum consequat nisl vel pretium lectus quam id. Risus pretium quam vulputate dignissim. Malesuada nunc vel risus commodo viverra maecenas accumsan. Arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc. Neque gravida in fermentum et sollicitudin. Lacus luctus accumsan tortor posuere ac ut. Dictum varius duis at consectetur lorem donec massa sapien faucibus. Lobortis scelerisque fermentum dui faucibus. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Non tellus orci ac auctor. Justo nec ultrices dui sapien eget mi proin sed libero. Sed felis eget velit aliquet sagittis id consectetur purus ut. Sit amet risus nullam eget. Nisi porta lorem mollis aliquam ut porttitor leo a diam. Etiam sit amet nisl purus in mollis nunc sed id. Magna etiam tempor orci eu. Neque vitae tempus quam pellentesque nec nam aliquam sem. Mattis pellentesque id nibh tortor id. Turpis tincidunt id aliquet risus feugiat in ante metus dictum.</p>
        <p>Senectus et netus et malesuada fames. Auctor neque vitae tempus quam pellentesque nec nam aliquam sem. Viverra ipsum nunc aliquet bibendum enim. Diam in arcu cursus euismod. Dictumst quisque sagittis purus sit amet. Massa ultricies mi quis hendrerit dolor magna eget est lorem. Tempor id eu nisl nunc. Ipsum dolor sit amet consectetur adipiscing elit. Facilisi nullam vehicula ipsum a arcu. Neque laoreet suspendisse interdum consectetur libero. Sed felis eget velit aliquet sagittis id consectetur purus. Mauris in aliquam sem fringilla ut morbi tincidunt augue. Dui id ornare arcu odio ut. Nulla aliquet enim tortor at auctor urna nunc id cursus. Faucibus purus in massa tempor nec feugiat nisl pretium. Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Sollicitudin aliquam ultrices sagittis orci a scelerisque purus semper eget. Odio eu feugiat pretium nibh. Fusce ut placerat orci nulla pellentesque dignissim enim. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi.</p>
      </div>
      <div className='ChatContainer'>
        <input type="text" id="chatBox"></input>
        <button onClick={send}>Send</button>
        <button onClick={showMessages}>Show messages</button>
      </div>
    </div>
  );
}

export default App;
